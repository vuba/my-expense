package com.vuba.myexpenses.baseservice.user;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collation = "user")
@Data
public class User {
    @MongoId
    private String id;
    private String firstName;
    private String lastName;
    private String customId;
}
