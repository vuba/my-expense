package com.vuba.myexpenses.baseservice.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    @Query("{'firstName':?0, 'lastName':?1}")
    Optional<User> findByFirstNameAndLastName(final String firstName, final String lastName);

    @Query(value = "{'firstName:?0}", fields = "{lastName: 1}")
    User findLastNamesOfUsersWIthFirstName(final String firstName);
}
