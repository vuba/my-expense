package com.vuba.myexpenses.baseservice.user;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/internal/v1")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String hello() {
        return "hello";
    }

    @PostMapping
    public ResponseEntity saveUser(UserRequest user) {
        userService.save(user);
        return ResponseEntity.ok().build();
    }
}
