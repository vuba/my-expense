package com.vuba.myexpenses.baseservice.user;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class UserRequestToUserMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(UserRequest.class, User.class)
                .byDefault()
                .register();
    }
}
