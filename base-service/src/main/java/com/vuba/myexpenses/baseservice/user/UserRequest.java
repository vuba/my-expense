package com.vuba.myexpenses.baseservice.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserRequest {
    @ApiModelProperty(value = "First name of sser", example = "John", required = true)
    private String firstName;
    @ApiModelProperty(value = "Last name of user", example = "Doe", required = true)
    private String lastName;
    @ApiModelProperty(value = "Custom unique user identifier in system", example = "user-1", required = true)
    private String customId;
}
