package com.vuba.myexpenses.baseservice;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
public class ExampleKafkaListener {

    @KafkaListener(topics = "#{'${spring.kafka.consumer.topic}'}")
    public synchronized void listen(@Header(KafkaHeaders.RECEIVED_TOPIC) String topic, ConsumerRecord<String, String> record){

    }
}
