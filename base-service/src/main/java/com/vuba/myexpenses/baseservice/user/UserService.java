package com.vuba.myexpenses.baseservice.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    private final UserRequestToUserMapper userRequestToUserMapper;

    private final UserRepository userRepository;

    public UserService(UserRequestToUserMapper userRequestToUserMapper, UserRepository userRepository) {
        this.userRequestToUserMapper = userRequestToUserMapper;
        this.userRepository = userRepository;
    }

    public void save(UserRequest userRequest) {
        LOG.debug("Request to save userRequest [{}]", userRequest);
        var user = userRequestToUserMapper.map(userRequest, User.class);
        LOG.debug("Mapped user [{}]", user);
        userRepository.save(user);
    }
}
